/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buchalkas_composition;

/**
 *
 * @author mxnte
 */
public class Dimensions {
    
    private int width;
    private int heigth;
    private int depth;

    public Dimensions(int width, int heigth, int depth) {
        this.width = width;
        this.heigth = heigth;
        this.depth = depth;
    }

    public int getWidth() {
        return width;
    }

    public int getHeigth() {
        return heigth;
    }

    public int getDepth() {
        return depth;
    }
    
    
    
}
