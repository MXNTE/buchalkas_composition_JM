package buchalkas_composition;

public class Motherboard {


private String model;
private String Manufacturer;
private int ramSlots;
private int cardSlots;
private String bios;
public void loadProgram(String programName){

    System.out.println(programName+"program is loading...");

}

    public Motherboard(String model, String Manufacturer, int ramSlots, int cardSlots, String bios) {
        this.model = model;
        this.Manufacturer = Manufacturer;
        this.ramSlots = ramSlots;
        this.cardSlots = cardSlots;
        this.bios = bios;
    }

    public String getModel() {
        return model;
    }

    public String getManufacturer() {
        return Manufacturer;
    }

    public int getRamSlots() {
        return ramSlots;
    }

    public int getCardSlots() {
        return cardSlots;
    }

    public String getBios() {
        return bios;
    }

    
}
